const DATE = "Date";
const SECTION = "Section";
const LINK = "Link";
const ISSUE = "Issue";

export const textParserFunction = (textInput) => {
  let template = {
    date: "",
    issue: "",
    sections: [],
  };

  const textObj = textInput.split("\n");
  let sectionCount = -1;

  Object.entries(textObj).forEach(function (key) {
    const textLine = key[1];

    if (textLine && textLine !== "") {
      let [lineKey, ...rest] = textLine.split(":");
      rest = rest.join(":");

      if (lineKey === DATE) {
        template.date = rest.trim();
      }

      if (lineKey === ISSUE) {
        template.issue = `${ISSUE} ${rest.trim()}`;
      }

      if (lineKey === SECTION) {
        template.sections.push({ heading: rest.trim(), links: [] });
        sectionCount = sectionCount + 1;
      }

      if (lineKey === LINK) {
        let [text, url] = rest.split(",");
        template.sections[sectionCount].links.push({
          text: text.trim(),
          url: url.trim(),
        });
      }
    }
  });

  return template;
};
