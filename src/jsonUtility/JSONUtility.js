import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CloseIcon from "@mui/icons-material/Close";
import FormControl from "@mui/material/FormControl";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import Snackbar from "@mui/material/Snackbar";
import { textParserFunction } from "./textParser";

export default function JSONUtility() {
  const [inputEntry, setInputEntry] = React.useState("");
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState("");

  const processTextInput = () => {
    try {
      const jsonOutput = textParserFunction(inputEntry);

      if (jsonOutput.date !== "") {
        navigator.clipboard.writeText(JSON.stringify(jsonOutput));
        setToastMessage("JSON copied to clipboard successfully");
      }
    } catch (e) {
      setToastMessage("Error generating JSON");
    }

    setShowToast(true);
  };

  const handleClose = () => setShowToast(false);

  const action = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  const inputInstructions = (
    <p>
      Enter newsletter issue in the following format:
      <br />
      Date: dd/mm/yyyy
      <br />
      Section: Section heading after colon
      <br />
      Link: Link text before comma, URL after comma
    </p>
  );

  return (
    <Box
      component="form"
      sx={{ display: "flex", flexWrap: "wrap" }}
      noValidate
      autoComplete="off"
    >
      <FormControl fullWidth sx={{ m: 1 }}>
        <div>{inputInstructions}</div>
        <TextField
          id="standard-multiline-static"
          label="Enter newsletter issue"
          multiline
          rows={25}
          sx={{ m: 1 }}
          onChange={(e) => setInputEntry(e.target.value)}
        />

        <Button onClick={processTextInput} variant="contained" sx={{ m: 1 }}>
          Process text
        </Button>
      </FormControl>

      <Snackbar
        open={showToast}
        autoHideDuration={3000}
        onClose={handleClose}
        message={toastMessage}
        action={action}
      />
    </Box>
  );
}
