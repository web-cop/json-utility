import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import "./App.css";
import JSONUtility from "./jsonUtility/JSONUtility.js";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
        <h4>JSON Parser</h4>
        <JSONUtility />
      </ThemeProvider>
    </div>
  );
}

export default App;
